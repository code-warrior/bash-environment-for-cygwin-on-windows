# Setting Up Your Bash Environment In Cygwin

Included in this repository are a handful of files used to re-create the Cygwin environment you’ll need in class. It’s assumed that you’ve downloaded and installed Cygwin. If you haven’t, get it from [here](https://cygwin.com/install.html).

## Installation

1. Locate your home folder, which is likely in `/cygdrive/c/cygwin/home/USER_NAME`, `/cygdrive/c/Users/USER_NAME`, or `C:\cygwin\home\USER_NAME`. Each of these paths points to the same folder, and `USER_NAME` is your Windows user name.
2. Open your Cygwin home folder.
3. Move all the files in the `bash-environment-for-cygwin-on-windows` folder — except for this `README.md` file — from this repository into your Cygwin home folder. If you can’t see the files, because your operating system hides files starting with a dot (`.`), use the `mv` command in Cygwin.
4. Restart Cygwin, or source the `.bash_profile` from inside your Cygwin home folder:

      `source .bash_profile`

## Slow Cygwin Startup?

If Cygwin is slow to start, read the following, which suggests two methods for fixing the problem:

[http://stackoverflow.com/questions/28410852/startup-is-really-slow-for-all-cygwin-applications](http://stackoverflow.com/questions/28410852/startup-is-really-slow-for-all-cygwin-applications)
